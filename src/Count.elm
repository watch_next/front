module Count exposing (..)

import Browser
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Json.Decode as D
import Request exposing (Response(..))



-- MAIN


main : Program () Model Msg
main =
    Browser.element
        { init = init
        , update = update
        , subscriptions = subscriptions
        , view = view
        }



-- MODEL


type alias Model =
    { response : Response Int }


init : () -> ( Model, Cmd Msg )
init _ =
    ( Request.init Model, requestCount )



-- UPDATE


type Msg
    = RequestMsg (Request.Msg Int)


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        RequestMsg m ->
            ( Request.update m model, Cmd.none )



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none



-- VIEW


view : Model -> Html Msg
view model =
    div []
        [ h1 [] [ text "Count" ]
        , Request.view model viewCount
        ]


viewCount : Int -> Html Msg
viewCount count =
    p [] [ text ("Count: " ++ String.fromInt count) ]



-- HTTP


requestCount : Cmd Msg
requestCount =
    Request.getJson
        "series/"
        (D.field "count" D.int)
        RequestMsg
