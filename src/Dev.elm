module Dev exposing (..)

import Http


authHeader : Http.Header
authHeader =
    Http.header "Authorization" "Basic SDpwYXNzd29yZA=="


baseUrl : String
baseUrl =
    "http://127.0.0.1:8000/watchnext/"


imgUrl : Maybe String -> String
imgUrl poster_path =
    case poster_path of
        Just path ->
            -- "https://image.tmdb.org/t/p/w500" ++ path
            "https://image.tmdb.org/t/p/w500/yKyLJmRAtyXEEYKOvPhKHXIcPq9.jpg"

        Nothing ->
            "https://ih1.redbubble.net/image.1027712254.9762/pp,840x830-pad,1000x1000,f8f8f8.u2.jpg"


nlUuid : String
nlUuid =
    "81cf4540-3cd0-474a-9864-36287f94bc36"


resourceUuid : String
resourceUuid =
    "2d1d7ecb-6462-4711-8b1c-c7c9f8bb07a5"
