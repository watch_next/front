module Models exposing (..)

import Json.Decode exposing (..)


type alias Resource a =
    { a
        | uuid : String
        , name : String
        , poster_path : Maybe String
        , viewing_status : Int
    }


type alias ResourceItem =
    { uuid : String
    , name : String
    , year : Int
    , poster_path : Maybe String
    , viewing_status : Int
    }


type alias ResourceDetail =
    { uuid : String
    , name : String
    , date : String
    , poster_path : Maybe String
    , viewing_status : Int
    , recommended_by : List ResourceItem
    , recommendations : List ResourceItem
    }


type alias Item =
    { resource : ResourceItem
    , score : Int
    , weight : Int
    , origin : Int
    }


type alias NextList =
    { uuid : String
    , name : String
    , next : Item
    }


type alias NextListDetail =
    { uuid : String
    , name : String
    , next : Item
    , nexts : List Item
    , picks : List Item
    }


resourceItemDecoder : Decoder ResourceItem
resourceItemDecoder =
    map5 ResourceItem
        (field "uuid" string)
        (field "name" string)
        (field "year" int)
        (field "poster_path" (nullable string))
        (field "viewing_status" int)


resourceDetailDecoder : Decoder ResourceDetail
resourceDetailDecoder =
    map7 ResourceDetail
        (field "uuid" string)
        (field "name" string)
        (field "date" string)
        (field "poster_path" (nullable string))
        (field "viewing_status" int)
        (field "recommended_by" (list resourceItemDecoder))
        (field "recommendations" (list resourceItemDecoder))


itemDecoder : Decoder Item
itemDecoder =
    map4 Item
        (field "resource" resourceItemDecoder)
        (field "score" int)
        (field "weight" int)
        (field "origin" int)


nextListDecoder : Decoder NextList
nextListDecoder =
    map3 NextList
        (field "uuid" string)
        (field "name" string)
        (field "next" itemDecoder)


nextListDetailDecoder : Decoder NextListDetail
nextListDetailDecoder =
    map5 NextListDetail
        (field "uuid" string)
        (field "name" string)
        (field "next" itemDecoder)
        (field "nexts" (list itemDecoder))
        (field "picks" (list itemDecoder))
