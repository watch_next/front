module Movies exposing (..)

import Browser
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Json.Decode as D
import Json.Encode as E
import Models exposing (ResourceItem, resourceItemDecoder)
import Paginate exposing (Pagination, Paginator)
import Request exposing (Response(..))
import Views



-- MAIN


main : Program () Model Msg
main =
    Browser.element
        { init = init Movie
        , update = update
        , subscriptions = subscriptions
        , view = view
        }



-- MODEL


type Kind
    = Movie
    | Serie


type alias Model =
    { kind : Kind
    , response : Response (Paginator ResourceItem)
    , pagination : Pagination
    }


init : Kind -> () -> ( Model, Cmd Msg )
init kind _ =
    let
        model =
            Paginate.init (Model kind)
    in
    ( model, requestResources model )



-- UPDATE


type Msg
    = PaginateMsg (Paginate.Msg ResourceItem)
    | GotViewing (Request.Msg Int)
    | PostViewing String Int


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        PaginateMsg m ->
            Paginate.update m model requestResources

        GotViewing _ ->
            ( model, requestResources model )

        PostViewing uuid viewing ->
            ( model, postViewing uuid viewing )



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none



-- VIEW


view : Model -> Html Msg
view model =
    let
        txt =
            case model.kind of
                Movie ->
                    "Movies"

                Serie ->
                    "Series"
    in
    div []
        [ h1
            []
            [ text txt ]
        , Paginate.view
            model
            PaginateMsg
            viewResources
        ]


viewResources : Paginator ResourceItem -> Html Msg
viewResources resources =
    div
        []
        (List.map (Views.resourceItem PostViewing) resources.results)



-- HTTP


requestResources : Model -> Cmd Msg
requestResources model =
    let
        endpoint =
            case model.kind of
                Movie ->
                    "movies/"

                Serie ->
                    "series/"
    in
    Paginate.getPaginatedJson
        endpoint
        resourceItemDecoder
        PaginateMsg
        model.pagination


postViewing : String -> Int -> Cmd Msg
postViewing uuid viewing =
    Request.postJson
        ("resources/" ++ uuid ++ "/viewing/")
        (E.object [ ( "status", E.int viewing ) ])
        (D.field "uuid" D.int)
        GotViewing
