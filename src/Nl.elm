module Nl exposing (..)

import Browser
import Dev exposing (imgUrl, nlUuid)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Models exposing (Item, NextListDetail, nextListDetailDecoder)
import Nls exposing (itemTitle)
import Request exposing (Response(..))



-- MAIN


main : Program () Model Msg
main =
    Browser.element
        { init = init
        , update = update
        , subscriptions = subscriptions
        , view = view
        }



-- MODEL


type alias Model =
    { uuid : String
    , response : Response NextListDetail
    }


init : () -> ( Model, Cmd Msg )
init _ =
    ( Request.init (Model nlUuid), getNl nlUuid )



-- UPDATE


type Msg
    = GotNl (Request.Msg NextListDetail)


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        GotNl m ->
            ( Request.update m model, Cmd.none )



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none



-- VIEW


view : Model -> Html Msg
view model =
    div []
        [ h1
            []
            [ text "NextList" ]
        , Request.view model viewNextListDetail
        ]


viewNextListDetail : NextListDetail -> Html Msg
viewNextListDetail nl =
    let
        r =
            nl.next.resource
    in
    div []
        [ h2 [] [ text nl.name ]
        , h3 [] [ text (itemTitle nl.next) ]
        , img [ src (imgUrl r.poster_path) ] []

        -- , Views.viewingToggle detail PostViewing
        , viewListItem "Nexts" nl.nexts
        , viewListItem "Picks" nl.picks
        ]


viewListItem : String -> List Item -> Html Msg
viewListItem title items =
    div []
        [ h3 [] [ text title ]
        , div [] (List.map viewItem items)
        ]


viewItem : Item -> Html Msg
viewItem item =
    div []
        [ img [ src (imgUrl item.resource.poster_path), width 100 ] []
        , p [] [ text (itemTitle item) ]
        ]



-- HTTP


getNl : String -> Cmd Msg
getNl uuid =
    Request.getJson
        ("next_lists/" ++ uuid)
        nextListDetailDecoder
        GotNl
