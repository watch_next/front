module Nls exposing (..)

import Browser
import Dev exposing (imgUrl)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Models exposing (Item, NextList, nextListDecoder)
import Paginate exposing (Pagination, Paginator)
import Request exposing (Response(..))



-- MAIN


main : Program () Model Msg
main =
    Browser.element
        { init = init
        , update = update
        , subscriptions = subscriptions
        , view = view
        }



-- MODEL


type alias Model =
    { response : Response (Paginator NextList)
    , pagination : Pagination
    }


init : () -> ( Model, Cmd Msg )
init _ =
    let
        model =
            Paginate.init Model
    in
    ( model, requestNls model )



-- UPDATE


type Msg
    = PaginateMsg (Paginate.Msg NextList)


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        PaginateMsg m ->
            Paginate.update m model requestNls



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none



-- VIEW


view : Model -> Html Msg
view model =
    div []
        [ h1
            []
            [ text "NextLists" ]
        , Paginate.view
            model
            PaginateMsg
            viewNextLists
        ]


viewNextLists : Paginator NextList -> Html Msg
viewNextLists nls =
    div
        []
        (List.map viewNextList nls.results)


viewNextList : NextList -> Html Msg
viewNextList nl =
    div []
        [ h2 [] [ text nl.name ]
        , img [ src (imgUrl nl.next.resource.poster_path) ] []
        , p [] [ text (itemTitle nl.next) ]
        ]


itemTitle : Item -> String
itemTitle item =
    item.resource.name
        ++ " ("
        ++ String.fromInt item.resource.year
        ++ ")"
        ++ " - "
        ++ String.fromInt item.score



-- HTTP


requestNls : Model -> Cmd Msg
requestNls model =
    Paginate.getPaginatedJson
        "next_lists/"
        nextListDecoder
        PaginateMsg
        model.pagination
