module Paginate exposing
    ( Msg
    , Pagination
    , Paginator
    , getPaginatedJson
    , init
    , requestUpdate
    , update
    , view
    )

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Json.Decode exposing (Decoder, field, int, map4, nullable, string)
import Request exposing (Response(..))



-- MODEL


type alias Paginated a result =
    { a
        | response : Response (Paginator result)
        , pagination : Pagination
    }


type alias Paginator result =
    { count : Int
    , next : Maybe String
    , previous : Maybe String
    , results : List result
    }


type alias Pagination =
    { page : String
    , pageSize : String
    , pageCount : Int
    }


init :
    (Response (Paginator result) -> Pagination -> Paginated a result)
    -> Paginated a result
init model =
    model Loading (Pagination "1" "10" 1)



-- UPDATE


type Msg result
    = Page String
    | PageSize String
    | GotPaginatedJson (Request.Msg (Paginator result))


update : Msg result -> Paginated a result -> (Paginated a result -> Cmd msg) -> ( Paginated a result, Cmd msg )
update msg paginated toCmd =
    case msg of
        Page page ->
            let
                p =
                    paginated.pagination

                pagination =
                    { p | page = page }

                up =
                    { paginated | pagination = pagination }
            in
            ( up, toCmd up )

        PageSize size ->
            let
                p =
                    paginated.pagination

                pagination =
                    { p | pageSize = size, page = "1" }

                up =
                    { paginated | pagination = pagination }
            in
            ( up, toCmd up )

        GotPaginatedJson m ->
            ( Request.customUpdate m paginated requestUpdate
            , Cmd.none
            )


requestUpdate : Paginated a result -> Paginator result -> Paginated a result
requestUpdate paginated paginator =
    let
        pageCount =
            case String.toInt paginated.pagination.pageSize of
                Just size ->
                    paginator.count // size + 1

                Nothing ->
                    1

        p =
            paginated.pagination

        pagination =
            { p | pageCount = pageCount }
    in
    { paginated | pagination = pagination }



-- VIEW


view : Paginated a result -> (Msg result -> msg) -> (Paginator result -> Html msg) -> Html msg
view paginated toMsg viewPaginator =
    let
        showPaginationView =
            case paginated.response of
                Success _ ->
                    case paginated.pagination.pageCount of
                        1 ->
                            False

                        _ ->
                            True

                _ ->
                    False

        requestView =
            Request.view paginated viewPaginator

        paginatedView =
            if showPaginationView then
                [ Html.map toMsg (viewPagination paginated.pagination)
                , requestView
                ]

            else
                [ requestView ]
    in
    div [] paginatedView


viewPagination : Pagination -> Html (Msg result)
viewPagination pagination =
    div []
        [ viewSelect
            "page-select"
            "Page number:"
            pagination.page
            Page
            (List.map
                String.fromInt
                (List.range 1 pagination.pageCount)
            )
        , viewSelect
            "page-size-select"
            "Items per page:"
            pagination.pageSize
            PageSize
            [ "10", "25", "50", "100", "250" ]
        ]


viewSelect : String -> String -> String -> (String -> Msg result) -> List String -> Html (Msg result)
viewSelect selectId selectLabel selectVal selectMsg options =
    div []
        [ label
            [ for selectId ]
            [ text selectLabel ]
        , select
            [ id selectId, name selectId, onInput selectMsg ]
            (List.map (viewOption selectVal) options)
        ]


viewOption : String -> String -> Html msg
viewOption selectedVal val =
    option [ value val, selected (selectedVal == val) ] [ text val ]



-- HTTP


getPaginatedJson :
    String
    -> Decoder result
    -> (Msg result -> msg)
    -> Pagination
    -> Cmd msg
getPaginatedJson url decoder toMsg pagination =
    Cmd.map toMsg (get url decoder pagination)


get : String -> Decoder result -> Pagination -> Cmd (Msg result)
get url decoder pagination =
    let
        pageQueryParam =
            if String.endsWith "/" url then
                "?page="

            else
                "&page="
    in
    Request.getJson
        (url
            ++ pageQueryParam
            ++ pagination.page
            ++ "&page_size="
            ++ pagination.pageSize
        )
        (paginatorDecoder decoder)
        GotPaginatedJson


paginatorDecoder : Decoder result -> Decoder (Paginator result)
paginatorDecoder decoder =
    map4 Paginator
        (field "count" int)
        (field "next" (nullable string))
        (field "previous" (nullable string))
        (field "results" (Json.Decode.list decoder))
