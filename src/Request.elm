module Request exposing
    ( Msg
    , Response(..)
    , customUpdate
    , getJson
    , init
    , postJson
    , update
    , view
    )

import Dev exposing (authHeader, baseUrl)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Http
import Json.Decode exposing (Decoder)
import Json.Encode exposing (Value)



-- MODEL


type alias Answered a data =
    { a | response : Response data }


type Response data
    = Loading
    | Failure
    | Success data


init : (Response data -> Answered a data) -> Answered a data
init model =
    model Loading



-- UPDATE


type Msg data
    = GotJson (Result Http.Error data)


update :
    Msg data
    -> Answered a data
    -> Answered a data
update (GotJson result) answered =
    case result of
        Ok response ->
            { answered | response = Success response }

        Err error ->
            errorUpdate error answered


customUpdate :
    Msg data
    -> Answered a data
    -> (Answered a data -> data -> Answered a data)
    -> Answered a data
customUpdate (GotJson result) answered custom =
    case result of
        Ok response ->
            custom { answered | response = Success response } response

        Err error ->
            errorUpdate error answered


errorUpdate :
    Http.Error
    -> Answered a data
    -> Answered a data
errorUpdate error answered =
    let
        _ =
            Debug.log "updateRequest Err" error
    in
    { answered | response = Failure }



-- VIEW


view : Answered a data -> (data -> Html msg) -> Html msg
view { response } viewData =
    case response of
        Loading ->
            p [] [ text "Loading ..." ]

        Failure ->
            p [] [ text "Failed to load !" ]

        Success data ->
            viewData data



-- HTTP


getJson :
    String
    -> Decoder data
    -> (Msg data -> msg)
    -> Cmd msg
getJson endpoint decoder toMsg =
    Cmd.map
        toMsg
        (Http.request
            { method = "GET"
            , headers = [ authHeader ]
            , url = baseUrl ++ endpoint
            , body = Http.emptyBody
            , expect = Http.expectJson GotJson decoder
            , timeout = Nothing
            , tracker = Nothing
            }
        )


postJson :
    String
    -> Value
    -> Decoder data
    -> (Msg data -> msg)
    -> Cmd msg
postJson endpoint body decoder toMsg =
    Cmd.map toMsg
        (Http.request
            { method = "POST"
            , headers = [ authHeader ]
            , url = baseUrl ++ endpoint
            , body = Http.jsonBody body
            , expect = Http.expectJson GotJson decoder
            , timeout = Nothing
            , tracker = Nothing
            }
        )
