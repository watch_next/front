module Resource exposing (..)

import Browser
import Dev exposing (imgUrl, resourceUuid)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Json.Decode as D
import Json.Encode as E
import Models exposing (ResourceDetail, ResourceItem, resourceDetailDecoder)
import Request exposing (Response(..))
import Views



-- MAIN


main : Program () Model Msg
main =
    Browser.element
        { init = init
        , update = update
        , subscriptions = subscriptions
        , view = view
        }



-- MODEL


type alias Model =
    { uuid : String
    , response : Response ResourceDetail
    }


init : () -> ( Model, Cmd Msg )
init _ =
    ( Request.init (Model resourceUuid), getResource resourceUuid )



-- UPDATE


type Msg
    = GotResource (Request.Msg ResourceDetail)
    | GotViewing (Request.Msg Int)
    | PostViewing String Int


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        GotResource m ->
            ( Request.update m model, Cmd.none )

        GotViewing _ ->
            ( model, Cmd.none )

        PostViewing uuid viewing ->
            ( model, postViewing uuid viewing )



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none



-- VIEW


view : Model -> Html Msg
view model =
    div []
        [ h1 [] [ text "Resource" ]
        , Request.view model viewResourceDetail
        ]


viewResourceDetail : ResourceDetail -> Html Msg
viewResourceDetail detail =
    div []
        [ h2 [] [ text detail.name ]
        , p [] [ text detail.date ]
        , img [ src (imgUrl detail.poster_path) ] []
        , Views.viewingToggle detail PostViewing
        , viewListResource "Recommended By" detail.recommended_by
        , viewListResource "Recommendations" detail.recommendations
        ]


viewListResource : String -> List ResourceItem -> Html Msg
viewListResource title resources =
    div []
        [ h3 [] [ text title ]
        , div [] (List.map (Views.resourceItem PostViewing) resources)
        ]



-- HTTP


getResource : String -> Cmd Msg
getResource uuid =
    Request.getJson
        ("resources/" ++ uuid)
        resourceDetailDecoder
        GotResource


postViewing : String -> Int -> Cmd Msg
postViewing uuid viewing =
    Request.postJson
        ("resources/" ++ uuid ++ "/viewing/")
        (E.object [ ( "status", E.int viewing ) ])
        (D.field "uuid" D.int)
        GotViewing
