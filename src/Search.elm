module Search exposing (..)

import Browser
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Models exposing (ResourceItem, resourceItemDecoder)
import Paginate exposing (Pagination, Paginator)
import Request exposing (Response(..))
import Views



-- MAIN


main : Program () Model Msg
main =
    Browser.element
        { init = init
        , update = update
        , subscriptions = subscriptions
        , view = view
        }



-- MODEL


type Kind
    = Movie
    | Serie


type alias Model =
    { name : String
    , response : Response (Paginator ResourceItem)
    , pagination : Pagination
    }


init : () -> ( Model, Cmd Msg )
init _ =
    ( Paginate.init (Model ""), Cmd.none )



-- UPDATE


type Msg
    = Search String
    | PaginateMsg (Paginate.Msg ResourceItem)


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Search "" ->
            init ()

        Search name ->
            let
                p =
                    model.pagination

                pagination =
                    { p | page = "1" }

                up =
                    { model | name = name, pagination = pagination }
            in
            ( up, requestResources up )

        PaginateMsg m ->
            Paginate.update m model requestResources



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none



-- VIEW


view : Model -> Html Msg
view model =
    div []
        [ h1
            []
            [ text "Search" ]
        , input
            [ type_ "text"
            , placeholder "search"
            , value model.name
            , onInput Search
            ]
            []
        , Paginate.view
            model
            PaginateMsg
            viewResources
        ]


viewResources : Paginator ResourceItem -> Html Msg
viewResources resources =
    div
        []
        (List.map Views.resourceItemWithoutViewing resources.results)



-- HTTP


requestResources : Model -> Cmd Msg
requestResources model =
    Paginate.getPaginatedJson
        ("resources/?search=" ++ model.name)
        resourceItemDecoder
        PaginateMsg
        model.pagination
