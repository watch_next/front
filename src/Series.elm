module Series exposing (..)

import Browser
import Movies exposing (Kind(..), Model, Msg, init, subscriptions, update, view)



-- MAIN


main : Program () Model Msg
main =
    Browser.element
        { init = init Serie
        , update = update
        , subscriptions = subscriptions
        , view = view
        }
