module Views exposing (..)

import Dev exposing (imgUrl)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Models exposing (Resource, ResourceItem)


resourceItemWithoutViewing : ResourceItem -> Html msg
resourceItemWithoutViewing r =
    let
        title =
            r.name ++ " (" ++ String.fromInt r.year ++ ")"
    in
    div []
        [ img [ src (imgUrl r.poster_path), width 100 ] []
        , p [] [ text title ]
        ]


resourceItem :
    (String -> Int -> msg)
    -> ResourceItem
    -> Html msg
resourceItem toMsg r =
    let
        title =
            r.name ++ " (" ++ String.fromInt r.year ++ ")"
    in
    div []
        [ img [ src (imgUrl r.poster_path), width 100 ] []
        , p [] [ text title ]
        , viewingToggle r toMsg
        ]


viewingToggle :
    Resource a
    -> (String -> Int -> msg)
    -> Html msg
viewingToggle r toMsg =
    let
        view =
            viewingButton r toMsg
    in
    div []
        [ view Enjoyed
        , view Neither
        , view Unliked
        ]


viewingButton :
    Resource a
    -> (String -> Int -> msg)
    -> Viewing
    -> Html msg
viewingButton r toMsg viewing =
    let
        ( status, label, activeColor ) =
            viewingData viewing

        postStatus =
            if status == r.viewing_status then
                4

            else
                status

        textColor =
            if status == r.viewing_status then
                activeColor

            else
                "yellow"
    in
    button
        [ onClick (toMsg r.uuid postStatus)
        , style "color" textColor
        ]
        [ text label
        ]


type Viewing
    = Enjoyed
    | Unliked
    | Neither
    | NotSeen


viewingData : Viewing -> ( Int, String, String )
viewingData viewing =
    case viewing of
        Enjoyed ->
            ( 1, "Enjoyed", "green" )

        Unliked ->
            ( 2, "Unliked", "red" )

        Neither ->
            ( 3, "Neither", "blue" )

        NotSeen ->
            ( 0, "", "" )
